(function ($) {
"use strict";

    if ($('.page-loader').length === 0) return;

	/* Preloader */
    $(window).on('load',function() {
		$('.page-loader').delay(350).fadeOut('slow');
	});	



})(jQuery);	
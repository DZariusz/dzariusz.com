    <footer>
        <div class="basic-footer text-center ptb-90">
            <div class="container">
                <div class="footer-logo mb-30">
                    <h3><a href="<?= home_url( '/' ); ?>" class="dzariusz"><?= __('DZariusz'); ?></a></h3>
                </div>
                <?php /*
                <div class="social-icon">
                    <a href="#"><i class="ion-social-facebook"></i></a>
                    <a href="#"><i class="ion-social-googleplus"></i></a>
                    <a href="#"><i class="ion-social-instagram"></i></a>
                    <a href="#"><i class="ion-social-dribbble"></i></a>
                </div>
                */ ?>
                <div class="footer-menu mt-30">
                    <nav>
                        <?php if ( has_nav_menu( 'main' ) ) : ?>
                            <?php
                                wp_nav_menu( array(
                                    'theme_location' => 'main',
                                    'depth'          => 1
                                ) );
                            ?>
                        <?php endif; ?>
                    </nav>
                </div>
                <div class="copyright mt-20">
                    <p>All copyright &copy; reserved by <a href="<?= home_url( '/' ); ?>" class="dzariusz-com">D<strong>Z</strong>ariusz.com</a> <?= date('Y'); ?></p>
                </div>
            </div>
        </div>
    </footer>

<?php wp_footer(); ?>

</body>
</html>

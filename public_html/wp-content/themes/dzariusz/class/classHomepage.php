<?php

/**
 * Created by PhpStorm.
 * User: DZariusz
 * Date: 2017-02-21
 * Time: 12:40
 */
class Homepage extends ACFinterface
{

    use AjaxPagination;

    const FILTER_category = 'category';


    public function __construct($id = false)
    {
        parent::__construct($id);

    }

    public static function make( $id = false ) {
        return new self( self::myID($id) );
    }



    public function readData($section = null, $subfield = false)
    {

        switch($section) {


            case 'featuredPrograms' :

                $this->prefix = 'programs_';
                $this->fields = array('display','cta','cta_url','title','title_bold');

                parent::readData();

                break;

            case 'testimonials' :

                $this->prefix = 'testimonials_';
                $this->fields = array('items', 'image');

                parent::readData();
                $this->readRelations($section);

                break;


            default:
                pre("Section `$section` do not exists. [". basename(__FILE__) .'/'. __LINE__ .']');
                break;
        }


        return $this;
    }

    public function readRelations($section = null)
    {
        switch ($section) {

            case 'testimonials':

                $this->prefix = 'testimonials_';
                $this->_relations = ['items' => ['prefix' => 'testimonial_', 'fields' => ['video_id', 'video_type']]];

                parent::readRelations();

                foreach ($this->items as $item) {
                    $item->image = get_the_post_thumbnail_url($item->ID, WPthumbnails::TESTIMONIAL);
                }

                break;

            default:
                pre("Section `$section` do not exists. [". basename(__FILE__) .'/'. __LINE__ .']');
                break;
        }

        return $this;

    }


    public function readRepeater($section = null)
    {

        switch($section) {

            case 'alternate':


                $this->prefix = 'alternate_';
                $this->fields = array('title', 'subtitle', 'media_type', 'banner', 'video');

                parent::readRepeaterEx($section, 'alternate_hero');


                break;

            case 'featuredPrograms':


                $this->prefix = 'programs_';
                $this->fields = array('program', 'cta', 'cta_url', 'cta_type', 'second');

                parent::readRepeaterEx($section, 'programs_featured');

                foreach($this->_repeater as $item) {
                    $item->program->image = get_the_post_thumbnail_url($item->program->ID, WPthumbnails::PROGRAMS);
                    $item->program->url = esc_url( $item->program->guid );
                }


                break;

            case 'metrics':

                $this->prefix = 'metric_';
                $this->fields = array('icon', 'number', 'text');

                parent::readRepeaterEx($section, 'metric_items');

                break;

            default:
                pre("Section `$section` do not exists. [". basename(__FILE__) .'/'. __LINE__ .']');
                break;
        }

        return $this;
    }




    public function applyQueryFilters()
    {

        global $wp_query;
        $wp_query2 = new WP_Query();

        $args = $this->getQueryFiltersArgs();

        $wp_query = $wp_query2;
        $wp_query->query($args);

    }

    public function getQueryFiltersArgs( $paged = null )
    {
        $this->id = $this->post_id;

        $this->paged = !is_null($paged) ? $paged : ( get_query_var('paged') ? get_query_var('paged') : 1);
        $offset = $this->paged == 1 ? 0 : ($this->paged == 2 ? $this->FIRST_PAGE  : ($this->paged - 2) * $this->PAGINATION +  $this->FIRST_PAGE );

        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => $this->paged ==1 ? $this->FIRST_PAGE :  get_option('posts_per_page'), //$this->PAGINATION
            'paged' => $this->paged,
            'offset' => $offset,
            'orderby'   => ['post_date' => 'DESC', 'ID' => 'DESC']
        );


        $args['tax_query'] = array();


        $tid =  empty($_GET[ self::FILTER_category ]) ? false : (int)$_GET[ self::FILTER_category ];
        if ($tid) {

            $args['tax_query'][] = array(
                'taxonomy' => self::FILTER_category,
                'field' => 'term_id',
                'terms' => $tid,
                'operator' => 'IN'
            );

        }

        return $args;
    }
}
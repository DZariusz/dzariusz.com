<?php

/**
 * Class ACFinterface
 *
 * version 2.1
 * 2017-06-01
 */

class ACFinterface
{

    // list all block that have bg,
    // that means - they by default do not have bottom margin
    // but margin will be added if after that block will be block without bg
    var $blocks_without_mb = [
        //block_name => ['field' => 'this value'] //then NO BG?
    ];


    protected $fields,
            /**
             * @var string
             * eg if you have hp_title, hp_subtitle fields, the prefix will be `hp_`
             * also, if fields are inside repeater, the repeater must have the name of prefix but without _ at the end
             */
              $prefix,
              $post_id;


    public $_repeater, //array of fields;
           $_relations, //array (relation_field => array(prefix => string, fields => array of fields to read))
           $_relations_field_name, //for return data

           $_flexible, //array( section => (object)( field => value )

            //I want to have info about block index and if it is first/last one
            //it will be array of: index:int, last:bool, first:bool
           $_flexible_info;

    public function __construct($id = false)
    {
        if (!$id) $id = get_the_ID();
        $this->post_id = $id;
        $this->fields = array();
        $this->prefix = '';


        $info = [
            'index' => -1,
            'first' => false,
            'last' => false,
            'margin_bottom' => true,
            'has_bg' => false
        ];

        $this->_flexible_info = (object)$info;
    }

    public static function make( $id = false ) {

        return new self($id);
    }

    public static function myID( $id = false ) {

        if ($id) return $id;

        if(is_home() || is_category()) {

            $id = get_option('page_for_posts');

        }elseif(is_front_page()) {

            $id = get_option('page_on_front');

        } else {
            $id = get_the_ID();
        }

        return $id;

    }

    /**
     * unset all dynamic fields, that was read by this class readers() functions.
     */
    private function resetFields() {

        $vars = get_object_vars( ACFinterface::make() );
        foreach (get_object_vars($this) as $k => $v) {
            if (!isset($vars[ $k ])) unset($this->$k);
        }

        $this->_relations = $this->_repeater = array();
    }


    /**
     * @param $bg_name string name of variable for handle url for image
     * @param $bg_size
     * @param $theme
     */
    public function read404($bg_name, $bg_size, $theme) {

        if (!$this->fields) {
            $this->fields = array('subtitle', 'title', $bg_name);
        }

        $this->title = __('Oops!', $theme) .'<strong>'. __('That page can’t be found.', $theme) .'</strong>';
        $this->subtitle = __('It looks like nothing was found at this location. Maybe try search?', $theme );


        $this->{$bg_name} = get_the_post_thumbnail_url( get_option('page_on_front'), $bg_size);


    }

    /**
     * reads data from DB/ACF
     * @param null $section for use in child classes
     * @return $this
     */
    public function readData($section = null, $subfield = false) {
        foreach($this->fields as $f) {
            //there is a case for close group, when we need to read `prefix` to ger group fields
            $field = empty(trim($f)) ? substr($this->prefix,0, -1) : $this->prefix . $f;
            if (empty(trim($f))) $f = $field;
            //if (isset($this->{$f})) continue; //NEED TO BE OVERRIDE! because it might be diffrent section
            $this->{$f} = $subfield ? get_sub_field($field, $this->post_id) : get_field($field, $this->post_id);
        }

        return $this;
    }

    public function readTaxonomyData($taxonomy, $tid) {
        foreach($this->fields as $f) {
            $this->{$f} = get_field($this->prefix . $f, $taxonomy .'_'. $tid);
        }
        return $this;
    }


    /**
     * for reading taxonomy repeater, set post_id to proper value
     * @see https://www.advancedcustomfields.com/resources/get-values-from-a-taxonomy-term/
     *
     * @param null $section for use in child classes
     * @return $this
     */
    public function readRepeater($section = null) 
    {
        return $this->readRepeaterEx($section, null);
    }
    
    public function readRepeaterEx($section = null, $repeater_field_name=null) 
    {

        $repeater = empty($repeater_field_name) ? substr($this->prefix,0,-1) : $repeater_field_name;

        $this->_repeater = [];

        if( have_rows($repeater, $this->post_id) ):

            // loop through the rows of data
            while ( have_rows($repeater, $this->post_id) ) : the_row();

                $data = new stdClass();
                foreach($this->fields as $f) {
                    //pre(get_sub_field($this->prefix . $f, $this->post_id), $this->prefix . $f);
                    $data->$f = get_sub_field($this->prefix . $f, $this->post_id);
                }
                $this->_repeater[] = $data;

            endwhile;

        endif;

        return $this;
    }

    public function  getRepeaterData() {
        return $this->_repeater;
    }

    /**
     * for ACF `relations` fields
     * object must have ->relatinos array set, where {key} is name of  relation field (without prefix!),
     * and items are additiona ACF fields to read,
     * default post fields like: title, featured image, category name will be read automatically
     * {key} must be read before by readData()
     *
     * @return array
     */
    public function readRelations($section = null) {

        if( empty($this->_relations) ) {
            pre('Missing `_relations` array for section: '. $section);
            return $this;
        }

        $return = array();

        foreach ($this->_relations as $relation_field => $params ) :

            if (!in_array($relation_field, $this->fields)) {
                pre('Relations field `'.$relation_field.'` must be first read by readData().');
                continue;
            }

            if (empty($this->$relation_field) || !is_array($this->$relation_field)) continue;

            foreach( $this->$relation_field as $post) : // variable must be called $post (IMPORTANT)

                setup_postdata($post);

                $data = array(
                    'title' => $post->post_title,
                    'ID' => $post->ID,
                    'url' => get_post_permalink($post->ID),
                    'excerpt' => $post->post_excerpt,
                );
                if ($post->post_type == 'post') {

                    $data['main_category'] = custom_getMainCategory($post->ID);

                }

                if (isset($params['fields']) && is_array($params['fields'])) {

                    foreach ($params['fields'] as $f) {
                        //pre("$params[prefix]$f => $post->ID");
                        $data[$f] = get_field($params['prefix'] . $f, $post->ID);
                    }

                }


                $return[] = (object)$data;

            endforeach;

            $this->$relation_field = $return;
            $this->_relations_field_name = $relation_field;

        endforeach;

        wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
    }

    public function getRelationsData() {

        return empty($this->_relations_field_name) ? [] : $this->{$this->_relations_field_name};

    }



    /**
     * @param $params array of (section => array(
     * + read => Data|Repeater|Relations: kay+value give us function that need to be user for read
     * + relations => array - if we read relations
     * + fields => array of fields names to read
     * + prefix => string
     */
    public function readFlexible($params) {

        $flexible = $this->prefix;
        // loop through the rows of data
        $last_id = -1;

        if( have_rows($flexible, $this->post_id ) ):

            $this->_flexible = array();


            while ( have_rows($flexible, $this->post_id) ) : the_row();

                $section = get_row_layout();

                $data = new ACFinterface($this->post_id);
                $data->section = $section;


                if (isset($params[ $section ]))
                foreach ($params[ $section ] as $block) {
                    switch( strtolower( $block['read']) )
                    {
                        case 'data':

                            $data->prefix = $block['prefix'];
                            $data->fields = $block['fields'];

                            $data->readData(null, true);

                            break;

                        case 'repeater':

                            $data->prefix = $block['prefix'];
                            $data->fields = $block['fields'];

                            $data->readRepeater();

                            break;

                        case 'relations':

                            $data->prefix = $block['prefix'];
                            $data->_relations = $block['relations'];

                            $data->readRelations($section);

                            break;

                        default:
                            pre('Read option `'. $block['read'] .'` do not exists. ['. basename(__FILE__) .'/'. __LINE__ .']');
                            break;

                    }
                }

                $last_id = $data->_flexible_info->index = count( $this->_flexible );
                $data->_flexible_info->first = empty( $this->_flexible );
                $this->_flexible[ $last_id ] = $data;


            endwhile;

            $this->_flexible[ $last_id ]->_flexible_info->last = true;

        endif;

        return $this;
    }

    /**
     * @return object with all variables
     */
    public function getFields() {
        $fields = array();
        foreach($this->fields as $f) {
            $fields[ $f ] = $this->$f;
        }
        return (object)$fields;
    }




    /**
     *
     * by default all blocks has margin bottom,
     * but when two block with bg are next to each other, first one do not need margin bottom.
     * also last block do not need
     */
    function setBottomMargin() {

        if (!$this->_flexible) return;
        $b = []; //blocks that may need bottom margin
        $prev = -1;

        //pre($this->blocks_without_mb);

        //go throw all blocks
        foreach ($this->_flexible as $k => $block)
        {
            if ( $block->_flexible_info->last ) {
                $this->_flexible[ $k ]->_flexible_info->margin_bottom = false;
            }


            if (!isset($this->blocks_without_mb[ $block->section ])) {
                $prev = $k;
                continue;
            }

            // CUSTOM CONDITIONS
            /* TODO

            //if editor doesnt have bg, then it doesn't count
            if ($block->section == 'editor' && !$block->background) {
                $prev = $k;
                continue;
            }

            //if vertical tabs doesnt have grey bg then ignore
            if ($block->section == T2_TEMPLATE::BLOCK_VERTICAL_TABS && !$block->grey) {
                $prev = $k;
                continue;
            }

            // */

            $this->_flexible[ $k ]->_flexible_info->has_bg = true;

            //so this section has BG,
            //if prev has also, then previous dont need margin bottom
            if ( isset ($this->_flexible[ $prev ]) && $this->_flexible[ $prev ]->_flexible_info->has_bg) {

                $this->_flexible[ $prev ]->_flexible_info->margin_bottom = false;

            }

            $prev = $k;

        }



    } // */




}
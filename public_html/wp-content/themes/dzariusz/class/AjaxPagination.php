<?php

/**
 * Created by PhpStorm.
 * User: DZariusz
 * Date: 2017-02-20
 * Time: 19:50
 */
trait AjaxPagination {

    protected $id; //current post ID

    //multiplication of 3
    public $FIRST_PAGE = 9;
    public $PAGINATION = 9;
    public $paged = 0;

    protected $count = -1; //num of all events accordingly to criteria eg filters
    protected $numOfPages = -1;


    /**
     * @param $post_id this is only to force provide Id and save is under $this->id
     * @param null $paged
     * @return mixed
     */
    abstract public function getQueryFiltersArgs( $paged = null );
    //example:
    /* {

        $this->id = $post_id;

        $this->paged = !is_null($paged) ? $paged : ( get_query_var('paged') ? get_query_var('paged') : 1);
        $offset = $this->paged == 1 ? 0 : ($this->paged == 2 ? $this->FIRST_PAGE  : ($this->paged - 2) * $this->PAGINATION +  $this->FIRST_PAGE );

        $args = array(
            'post_type' => CPT_EVENT,
            'post_status' => 'publish',
            'posts_per_page' => $this->paged ==1 ? $this->FIRST_PAGE :  $this->PAGINATION, //$wp_query->query_vars['posts_per_page'],
            'paged' => $this->paged,
            'offset' => $offset,
            'orderby'   => ['meta_value_num' => 'ASC', 'ID' => 'ASC'],
            'meta_key'  => 'event_date',
        );


        $args['tax_query'] = array(
            array(
                'taxonomy' => CTT_EVENT_CATEGORY,
                'field' => 'term_id',
                'terms' => classEvent::IN_PERSON_TAXID,
                'operator' => 'IN'
            ));

        $args['meta_query'] = array(array(
            'key' => 'event_date',
            'value' => date('Y-m-d H:i:s'),
            'compare' => '>='
        ));

        $tid =  empty($_GET[ self::FILTER_event_type ]) ? false : (int)$_GET[ self::FILTER_event_type ];
        if ($tid) {

            $args['tax_query'][] = array(
                'taxonomy' => CTT_EVENT_TYPE,
                'field' => 'term_id',
                'terms' => $tid,
                'operator' => 'IN'
            );

        }

        $state =  empty($_GET[ self::FILTER_state ]) ? null : $_GET[ self::FILTER_state ];
        if (isset( $this->states[ $state ] )) {

            $args['meta_query'] = array(array(
                'key' => self::FILTER_state,
                'value' => $state,
                'compare' => '='
            ));

        }


//pre($args);

        return $args;

    } // */


    public function getNumOfPages() {

        if ($this->numOfPages >=0 ) return $this->numOfPages;

        $this->numOfPages = 1;
        $c = $this->getCountOfAll() - $this->FIRST_PAGE;

        if ($c>0) {
            $this->numOfPages += ceil($c / $this->PAGINATION);
        }

        return $this->numOfPages;

    }

    public function getCountOfAll() {

        if ($this->count >=0 ) return $this->count;

        $args = $this->getQueryFiltersArgs();

        unset($args['offset']);
        $args['posts_per_page'] = -1;

        $q = new WP_Query( $args );
        $this->count = $q->found_posts;

        return $this->count;
    }

    public function getRelPrevNext() {

        $links = '';
        if ($this->getNumOfPages() <= 1) return $links;

        if ($this->paged > 1) {
            $q = $_GET;
            $q['paged'] = $this->paged - 1;
            unset( $_GET['page'] );

            $params = [];
            foreach ($q as $k=>$v) $params[] = "$k=$v";

            $links .= '<link rel="prev" href="'. get_permalink( $this->id ) .'?'. implode('&', $params) .'">';
        }

        if ($this->paged < $this->getNumOfPages()) {
            $q = $_GET;
            $q['paged'] = $this->paged + 1;
            unset( $_GET['page'] );

            $params = [];
            foreach ($q as $k=>$v) $params[] = "$k=$v";

            $links .= '<link rel="next" href="'. get_permalink( $this->id ) .'?'. implode('&', $params) .'">';
        }

        return $links;
    }

}
<?php

/**
 * Created by PhpStorm.
 * User: DZariusz
 * Date: 2017-02-20
 * Time: 19:50
 */
class PageContent extends ACFinterface
{


    public function __construct($id = false)
    {
        parent::__construct($id);

        $this->blocks_without_mb['testimonials_block'] = [];
        $this->blocks_without_mb['tabs_block'] = [];
        $this->blocks_without_mb['custom_tabs_block'] = [];
        $this->blocks_without_mb['callout_block'] = [];

    }

    public static function make( $id = false ) {
        return new self( self::myID( $id ));
    }

    /**
     * @param $section
     * @return $this
     */
    public function readData($section = null, $subfield = false)
    {
        switch ($section) {

            default:
                pre("Section `$section` do not exists. [". basename(__FILE__) .'/'. __LINE__ .']');
                break;
        }

        return $this;
    }

    public function HasFlexibleContent()
    {
        return have_rows('flexible_content', $this->post_id);
    }
            
    public function readFlexible($params = null)
    {
        $this->prefix = 'flexible_content';

        $params = array(

            'media_block' => array(array(
                'read' => 'data',
                'prefix' => 'media_',
                'fields' => array('type', 'image', 'signature', 'video')
            )),

            'testimonials_block' => array(array(
                'read' => 'data',
                'prefix' => 'testimonials_',
                'fields' => array('image', 'testimonials')
            ),array(
                'read' => 'relations',
                'prefix' => 'testimonials_testimonials_',
                'relations' => array('testimonials' => ['fields' => ['cta'], 'prefix' => 'testimonial_'])
            )),


            'form_block' => array(array(
                'read' => 'data',
                'prefix' => 'form_',
                'fields' => array('')
            )),

            'callout_block' => array(array(
                'read' => 'data',
                'prefix' => 'callout_',
                'fields' => array('icon', 'text', 'cta')
            )),



            'custom_tabs_block' => array(array(
                'read' => 'data',
                'prefix' => 'tabs_',
                'fields' => array('type', 'title', 'subtitle', 'cta')
            ),array(
                'read' => 'repeater',
                'prefix' => 'tabs_items_',
                'fields' => array('label', 'title', 'title_optional', 'content', 'cta', 'image')
            )),

        );

        parent::readFlexible($params);

        $this->setBottomMargin();

        return $this;
    }


}
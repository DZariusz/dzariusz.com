<?php

/**
 * Created by PhpStorm.
 * User: DZariusz
 * Date: 2017-02-21
 * Time: 12:40
 */
class GlobalElements extends ACFinterface
{

    public function __construct($id = false)
    {
        parent::__construct($id);
    }

    public static function make($id = false)
    {
        return new self( self::myID($id) );
    }


    public function readData($section = null, $subfield = false)
    {

        switch($section) {


            case 'banner' :

                if (is_404() || is_search()) {



                } else {

                    $this->prefix = 'banner_';
                    $this->fields = array('subtitle',
                        'cta','cta_url' //homepage fields
                    );

                    parent::readData();

                    $this->fields[] = 'title';
                    $this->title = get_the_title($this->post_id);

                    $this->fields[] = 'banner';

                    $this->banner = get_the_post_thumbnail_url($this->post_id, WPthumbnails::BANNER);

                }
                break;

            default:
                pre("Section `$section` do not exists. [" . basename(__FILE__) . '/' . __LINE__ . ']');
                break;
        }


        return $this;
    }


}
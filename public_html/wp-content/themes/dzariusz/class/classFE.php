<?php

/**
 * Created by PhpStorm.
 * User: DZariusz
 * Date: 2017-05-15
 * Time: 19:24
 */
class FE
{

    const CONTACT_ID = 473;

    /**
     * Print value if its not empty
     *
     * @param $val
     * @param string $before
     * @param string $after
     * @param string $esc attr|html|null|br
     */
    public static function __($val, $before = '', $after = '', $esc = null) {
        if (empty($val)) return '';

        $f = 'esc_'.$esc;

        if ($esc == 'br') {
            $val = esc_html($val);
            $val = nl2br($val);
        } elseif ($esc && function_exists($f)) {
            $val = $f($val);
        }
        return "$before$val$after";
    }


    public static function ctaVideo($object, $attr = [], $container = '', $esc_html = true, $embed = false) {
        if (empty($object)) return '';
        $object = (object)$object;

        if (empty($object->video_id)) return '';
        if (empty($object->video_type)) return '';


        $f = $object->video_type . 'Url';

        $object->cta_url = self::$f($object->video_id, $embed);


        return self::cta($object, $attr, $container, $esc_html);

    }

    public static function youtubeUrl($v, $embed = false) {
        return ($embed ? 'https://www.youtube.com/embed/' : 'https://www.youtube.com/watch?v='). $v;
    }
    public static function vimeoUrl($v, $embed = false) {
        return ($embed ? 'https://player.vimeo.com/video/' : 'https://vimeo.com/'). $v;
    }

    public static function embedPlayer($object, $w = 1200, $h = 676) {

        if (empty($object)) return '';
        $object = (object)$object;


        if (empty($object->video_id)) return '';
        if (empty($object->video_type)) return '';

        $f = $object->video_type . 'Url';

        $url = self::$f($object->video_id, true);

        $html = '';

        switch ($object->video_type) {
            case 'vimeo':
                $html = '<iframe src="'. $url .'?title=0&byline=0&portrait=0" width="'. $w .'" height="'. $h .'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                break;

            case 'youtube':
                $html = '<iframe src="'. $url .'?rel=0&amp;showinfo=0" width="'. $w .'" height="'. $h .'" frameborder="0" allow="autoplay; encrypted-media" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                break;

        }

        return $html;

    }



    public static function getStyleColor($color) {
        if (!$color) return '';

        return ' style="color: '. $color .';"';
    }

    public static function getStyleBGcolor($color) {
        if (!$color) return '';

        return ' style="background-color: '. $color .';"';
    }

    public static function getStyleBorderColor($color) {
        if (!$color) return '';

        return ' style="border-color: '. $color .';"';
    }

    public static function getStyleBGimg($image, $size = '', $styles = '') {
        if (!$image) {
            return $styles ? "style='$styles'" :'';
        }

        if (is_array($image)) $src = $size ? $image['sizes'][ $size ] : $image['url'];
        else $src = $image;

        return ' style="background-image: url('. esc_url($src) .'); '. $styles .'"';
    }

    private static function getImageAttr($image) {

        $basename = basename($image['url']);

        return
            ' alt="'. (!empty($image['alt']) ? $image['alt'] : $basename).'" '.
            ' title="'. (!empty($image['title']) ? $image['title'] : $basename).'" '.
            (!empty($image['class']) ? "class='$image[class]'" : '');
    }

    private static function getTagImgImg($image, $size = '')
    {
        $src = $size ? $image['sizes'][ $size ] : $image['url'];

        $w = $size ? $image['sizes'][ $size .'-width' ] : $image['width'];
        $h = $size ? $image['sizes'][ $size .'-height' ] : $image['height'];

        return '<img src="'. $src .'" '.
            self::getImageAttr($image).
            ($w ? ' width="'. $w .'" ' : '').
            ($h ? ' height="'. $h .'" ' : '').
            ' />';
    }

    private static function getTagImgSvg($image, $size = '', $color = '#000')
    {
        $src = $image['url'];

        $size = custom_getImageSize($size);

        $w = $size ? $size['width'] : '';
        $h = $size ? $size['height'] : '';

        //pre($image);
        $rand = mt_rand(100, 999);

        return '
                <style>
                svg.'.$image['title'].$rand.' * {
                 fill: '.$color.';'.
            '}
                svg.'.$image['title'].$rand.' {'.
            ($w ? ' width:'. $w .'px; ' : '').
            ($h ? ' height:'. $h .'px; ' : '').
            '}
                </style>
                <img src="'. $src .'" '.
            self::getImageAttr($image).
            ($w ? ' width="'. $w .'" ' : '').
            ($h ? ' height="'. $h .'" ' : '').
            ' class="style-svg '.$image['title'].$rand.'" />';
    }

    /**
     * @param $image array from ACF
     * @param $size string
     * @param $color string BG color for SVG or class for simple image
     *
     * @return string
     */
    public static function getTagImg($image, $size = '', $attr = '') {
        if (!$image) return '';

        if (strpos($image['mime_type'], 'svg') === false) {
            if ($attr) $image['class'] = $attr;
            return self::getTagImgImg($image, $size);
        } else {
            $color = $attr ? $attr : '#FFF';
            return self::getTagImgSvg($image, $size, $color);
        }


    }

    public static function getFeaturedImgTag($post_id, $size = 'thumbnail', $attr = null) {
        if (!$post_id) return '';

        $attr_id =  get_post_thumbnail_id($post_id);

        return $attr_id ? wp_get_attachment_image($attr_id, $size, false, $attr) : '';

    }


    public static function ctaEx($cta, $url, $attr = [], $container = '', $esc_html = true) {
        $object = ['cta' => $cta, 'cta_url' => $url];
        return self::cta((object)$object,$attr, $container, $esc_html);
    }

    /**
     * @param $url
     * @param $cta
     * @param string $class class for <a> OR array of attributes
     * @param string $container if there is container, make sure you set space after tag eg <div >
     */
    public static function cta($object, $attr = [], $container = '', $esc_html = true) {
        $object = (object)$object;
        if (!$object->cta_url || !$object->cta) return '';

        if (!is_array($attr)) {
            $tmp = $attr;
            $attr = [];
            $attr['class'] = $tmp;
        }

        //add nofollow
        $isEmail = strpos($object->cta_url, 'mailto:') !== false;
        $isTel = strpos($object->cta_url, 'tel:') !== false;

        if (!$isEmail && !$isTel) {
            $p = parse_url($object->cta_url);
            $host_search = $_SERVER['HTTP_HOST'];
            if (!empty($p['host']) && (strpos($p['host'], $host_search) === false)) {
                if (empty($attr['rel'])) $attr['rel'] = 'nofollow';
                if (empty($attr['target'])) $attr['target'] = '_blank';
            }
        }


        $html = '';
        foreach ($attr as $k=>$v) {
            $html .= " $k='$v' ";
        }
        $end = $container ? '</'. substr(explode(' ', $container)[0], 1) .'>': '';

        if ($esc_html) $object->cta = esc_html($object->cta);

        return "$container<a href='". esc_url($object->cta_url) ."' $html>". $object->cta. "</a>$end";

    }

    public static function cta_email($email, $cta, $attr = [], $container = '') {
        if (!$email || !$cta) return '';

        $email = "mailto:$email";
        return self::cta($email, $cta, $attr, $container);
    }

    public static function cta_phone($phone, $cta, $attr = [], $container = '') {
        if (!$phone || !$cta) return '';

        $t = str_replace([' ','-'], ['',''], $phone);
        $phone = "tel:$t";
        return self::cta($phone, $cta, $attr, $container);
    }



    /**
     * @param string $path relative path base on theme directory
     */
    public static function findJSfiles($path = '/assets/js/') {
        $js = [];
        $search_path = get_template_directory() . $path;

        if (file_exists( $search_path )) {
            $files = scandir($search_path);
            foreach ($files as $f) {
                $ex = explode('.', $f);
                if (end($ex) != 'js') continue;
                $js[] = $f;
            }
        } else {
            pre('WARNING: path do not exist: '. $search_path);
        }

        return $js;
    }

    public static function browser() {

        $str = strtolower( var_export( $_SERVER,1 ) );

        //pre($str, '', true);
        $browser = false;
        foreach (['edge','chrome','firefox','safari','msie','trident'] as $b) {
            if ( !$browser && (strpos( $str, $b) !== false) ) $browser = $b;
        }

        if ( in_array($browser, ['msie','trident','edge']) ) {
            $browser = 'ie';
        }

        return $browser;


    }


    /**
     * helper for include script - it set automatic version base on time
     * @param $handle
     * @param $src
     * @param array $deps
     * @param bool $in_footer
     */
    public static function enqueue_script($handle, $src, $deps = array(), $in_footer = true) {
        $f = get_template_directory() . $src;
        if ( file_exists($f) ) {
            $t = filemtime( $f );
            wp_enqueue_script($handle, get_theme_file_uri($src), $deps, $t, $in_footer);
        } else {
            pre('WARNING: file do not exist: '. $f);
        }
    }
    public static function enqueue_style($handle, $src = '', $deps = array(), $media = 'all') {
        $f = get_template_directory() . $src;
        if ( file_exists($f) ) {
            $t = filemtime($f);
            wp_enqueue_style($handle, get_theme_file_uri($src), $deps, $t, $media);
        }
        else {
            pre('WARNING: file do not exist: '. $f);
        }
    }

    /**
     * return classes for flexible contect block
     * @param ACFinterface $block
     * @return string
     */
    public static function blockClasses(ACFinterface $block) {

        $base = 'acf-flex-block';
        $class = [];
        if ($block->_flexible_info->index >= 0) {
            $class[] = $base;
            $class[] = $base .'-'. $block->_flexible_info->index;
            if ($block->_flexible_info->first) $class[] = $base .'-first';
            elseif ($block->_flexible_info->last) $class[] = $base .'-last';

            if ($block->_flexible_info->margin_bottom) $class[] = $base .'-mb';
        }

        return implode(' ', $class);

    }

    public static function emptyContent($content) {
        return empty(trim(str_replace('&nbsp;','',strip_tags($content))));
    }

    public static function slickNav($class = '') {
        return '<div class="slick-nav '.$class.'"></div>';
    }


    public static function getPageNumber() {

        $page = get_query_var('paged');
        return !$page ? 1 : $page;

    }


    /**
     * @param array $requireFields
     * @param bool $and, if true all fields must be non empty, if false, only one need to be
     * @return bool
     */
    public static function isContentBlockEmpty(Array $requireFields, $and = true) {

        global $CONTENT_BLOCK;
        if (empty($CONTENT_BLOCK)) return true;

        $empty = !$and;

        foreach ($requireFields as $f) {
            if ($and && empty($CONTENT_BLOCK->$f)) return true;
            if (!$and && !empty($CONTENT_BLOCK->$f)) $empty = false;
        }

        return $empty;

    }

}
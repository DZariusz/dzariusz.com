<?php

/**
 * instalation:
 *
 * add this line:
 * WPthumbnails::init();
 *
 * in function php under:
 * add_theme_support( 'post-thumbnails' );
 *
 */

/**
 * please add any thumbnails sizes here
 */
class WPthumbnails
{
    //default WP
    const FULL = 'full'; // Original resolution
    const LARGE = 'large'; // Large resolution for CMS full width
    const MEDIUM = 'medium'; //main slide image
    const THUMB = 'thumbnail'; //dimensions set in function.php


    //custom:
    const BANNER = 'banner';
    const PORTFOLIO = 'portfolio';



	public static function init() 
	{
        set_post_thumbnail_size(120, 120, 1);
        update_option('thumbnail_size_w', 120);
        update_option('thumbnail_size_h', 120);
        update_option('thumbnail_crop', true);

        update_option('large_size_w', 1280);
        update_option('large_size_h', 1280);

        update_option('medium_size_w', 367);
        update_option('medium_size_h', 367*2);
        update_option('medium_crop', false);


        add_image_size( self::BANNER, 1920, 700, 0);
        add_image_size( self::PORTFOLIO, 270, 270, 1);



	}

	
}


<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage DZariusz
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>

    <?php if (strpos($_SERVER['HTTP_HOST'], '.com') !== false) : ?>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-117503376-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-117503376-1');
        </script>

    <?php endif; ?>

<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

    <?php $favicon_path = esc_url(get_template_directory_uri() .'/assets/images/favicon'); ?>

    <link rel="apple-touch-icon" sizes="76x76" href="<?= $favicon_path; ?>/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= $favicon_path; ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $favicon_path; ?>/favicon-16x16.png">
    <link rel="manifest" href="<?= $favicon_path; ?>/site.webmanifest">
    <link rel="mask-icon" href="<?= $favicon_path; ?>/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="<?= $favicon_path; ?>/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">


<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>


    <!-- Add your site or application content here -->
    <!-- PRELOADER -->
    <div class="page-loader" >
        <div class="loader"><?= __('Loading...', THEME_SLUG); ?></div>
    </div>
    <!-- /PRELOADER -->

    <?php get_template_part( 'template-parts/global/main-menu'); ?>



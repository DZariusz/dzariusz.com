<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage DZariusz
 * @since 1.0
 * @version 1.0
 */

get_header();

get_template_part( 'template-parts/global/top');
//Homepage::make()->applyQueryFilters();

if ( have_posts() ) : ?>

    <div class="basic-portfolio-area ptb-90">
        <div class="container">

            <?php get_template_part( 'template-parts/global/cat-filters'); ?>

            <div id="portfolio-grid" class="row-portfolio portfolio-grid-4">

                <?php while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/post/content', 'portfolio');

                endwhile; ?>

            </div>
            <?php /*
            <div class="view-more mt-20 text-center cb">
                <a class="btn btn-large" href="<?= esc_url(home_url('/portfolio')); ?>">View More</a>
            </div> */ ?>
        </div>
    </div>
<?php endif; ?>

<?php get_footer(); ?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage DZariusz
 * @since 1.0
 * @version 1.0
 */

get_header();

get_template_part( 'template-parts/global/top');


//first, lets print main ocntent from main editor
while ( have_posts() ) : the_post();

    if (FE::emptyContent( $post->post_content )) {
        continue;
    } ?>

    <main id="main" class="site-main" role="main">
        <?php get_template_part( 'template-parts/page/content', 'page' ); ?>
    </main><!-- #main -->

<?php
endwhile; // End of the loop.


$SITE_CONTENT = PageContent::make();
global $CONTENT_BLOCK;


if ($SITE_CONTENT->HasFlexibleContent())
{
    foreach ($SITE_CONTENT->readFlexible()->_flexible as $CONTENT_BLOCK)
    {
        if (empty($CONTENT_BLOCK) || empty($CONTENT_BLOCK->section)) continue;


        $t = 'template-parts/page/fc-' . $CONTENT_BLOCK->section . '.php';
        if (locate_template( $t ) != '')
        {
            // yep, load the page template
            get_template_part( substr( $t, 0, -4));
        }
        else
        {
            // nope, load the content
            pre('Missing template part: '. $t);
        }
    }


}


get_footer();

<?php
/**
 * Created by PhpStorm.
 * User: dariusz@bfm
 * Date: 2017-02-15
 * Time: 18:47
 */



/**
 * Enqueue scripts and styles.
 */
function custom_scripts() {

    if ($GLOBALS['pagenow'] == 'wp-login.php' || is_admin()) return;

    // Theme stylesheet.
    #wp_enqueue_style( 'aarp-style', get_stylesheet_uri() );


    // Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
    /*if ( is_customize_preview() ) {
        wp_enqueue_style( 'aarp-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'aarp-style' ), '1.0' );
        wp_style_add_data( 'aarp-ie9', 'conditional', 'IE 9' );
    }//*/



    // Add custom fonts, used in the main stylesheet.
    #wp_enqueue_style( 'aarp-fonts', aarp_fonts_url(), array(), null );

    // Theme stylesheet.
    #wp_enqueue_style( 'liptomilgoldfx-style', get_stylesheet_uri(), array('liptomilgoldfx-bundle') );
    //FE::enqueue_style( 'liptomilgold-slick', '/assets/js/lib/slick/slick.css', array( ) );
    //FE::enqueue_style( 'liptomilgold-fancybox', '/assets/js/lib/fancybox-3.0/dist/jquery.fancybox.min.css', array( ) );
    //FE::enqueue_style( 'prettyphoto-css', '/assets/prettyPhoto_compressed_3.1.6/css/prettyPhoto.css', array( ) );
    FE::enqueue_style( 'dzariusz-all', '/assets/css/all.css', array() );



    //FE::enqueue_script( 'liptomilgold-slick', '/assets/js/lib/slick/slick.min.js', array( 'jquery' ));
    //FE::enqueue_script( 'liptomilgold-fancybox', '/assets/js/lib/fancybox-3.0/dist/jquery.fancybox.min.js', array( 'jquery' ));
    //FE::enqueue_script( 'liptomilgold-marquee', '/assets/js/lib/marquee/gistfile1.js', array( 'jquery' ));

    //wp_enqueue_script('aarp-validate', '//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js', ['jquery'], -1, true);

    //FE::enqueue_script( 'slick-js', '../vendors/slick/slick.min.js', array( 'jquery' ));
    //FE::enqueue_style( 'slick-css', '../vendors/slick/slick.css');


    //foreach (FE::findJSfiles('/vendors/') as $js) {
        //FE::enqueue_script( $js, '/vendors/'.$js, array( 'jquery' ));
    //}
    FE::enqueue_script( 'js-console', '/vendors/_console.js', array());
    FE::enqueue_script( 'js-grayscale', '/vendors/grayscale.js', array());
    FE::enqueue_script( 'js-owl-carousel', '/vendors/owl-carousel.js', array('jquery'));
    ////FE::enqueue_script( 'js-headroom', '/vendors/headroom.min.js', array('jquery'));
    //FE::enqueue_script( 'js-counterup', '/vendors/counterup.js', array('jquery'));
    //FE::enqueue_script( 'js-waypoints', '/vendors/waypoints.js', array('jquery'));
    FE::enqueue_script( 'js-imagesloaded', '/vendors/imagesloaded.pkgd.min.js', array('jquery'));
    FE::enqueue_script( 'js-isotope', '/vendors/isotope.pkgd.min.js', array('jquery'));
    //FE::enqueue_script( 'js-YTPlayer', '/vendors/YTPlayer.js', array('jquery'));
    FE::enqueue_script( 'js-jquery.magnific-popup', '/vendors/jquery.magnific-popup.min.js', array('jquery'));
    FE::enqueue_script( 'js-scrollup', '/vendors/scrollup.js', array('jquery'));
    //FE::enqueue_script( 'js-animate-headline', '/vendors/animate-headline.js', array('jquery'));
    FE::enqueue_script( 'jquery.meanmenu', '/vendors/jquery.meanmenu.js', array( 'jquery' ));

    foreach (FE::findJSfiles('/assets/js/') as $js) {
        FE::enqueue_script( $js, '/assets/js/'.$js, array( 'jquery' ));
    }


    // Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.

   // FE::enqueue_style( 'liptomilgold-ie','/assets/css/ie.css', array( 'liptomilgold-all' ));
    //wp_style_add_data( 'liptomilgold-ie', 'conditional', 'IE' );




    #wp_deregister_style("gforms_css");

}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );





/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function custom_body_classes( $classes ) {

    // Add class if the site title and tagline is hidden.
    $classes[] = 'browser-'.FE::browser();


    return $classes;
}
add_filter( 'body_class', 'custom_body_classes' );

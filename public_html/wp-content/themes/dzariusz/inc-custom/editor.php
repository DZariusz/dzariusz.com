<?php

function wpdocs_theme_add_editor_styles() {
    add_editor_style( 'assets/css/editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );


/**
 * remove editor for certain posts/pages
 */
add_action('init', 'remove_editor_init');
function remove_editor_init() {

    //remove by default
    //remove_post_type_support( 'post', 'editor' );

    // If not in the admin, return.
    if ( ! is_admin() ) {
        return;
    }

    $remove = false;

    // Get the post ID on edit post with filter_input super global inspection.
    $current_post_id = filter_input( INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT );
    // Get the post ID on update post with filter_input super global inspection.
    $update_post_id = filter_input( INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT );


    // Check to see if the post ID is set, else return.
    if ( isset( $current_post_id ) ) {
        $post_id = absint( $current_post_id );
    } else if ( isset( $update_post_id ) ) {
        $post_id = absint( $update_post_id );
    } else {

        //no id?

        return;
    }

    if (get_option('page_on_front') == $post_id) $remove = true;
    //if (get_option('page_for_posts') == $post_id) $remove = true;



    // Don't do anything unless there is a post_id.
    if ( isset( $post_id ) )
    {

        $p = get_post($post_id);

        // Example of removing page editor for page-your-template.php template.
        if (is_object($p) && in_array($p->post_type, array( /* CPT_EVENT */) ))
        {
            //remove_post_type_support( $p->post_type, 'editor' );
        } // */

    }


    if ($remove) {
        remove_post_type_support( 'page', 'editor' );
    }

}



// Callback function to insert 'styleselect' into the $buttons array
//install tinymce advanced plugin! and you can do it there
/* function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Register our callback to the appropriate filter
add_filter( 'mce_buttons_2', 'my_mce_buttons_2' ); // */


// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {

    $init_array['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;';

    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings

        array(
            'title' => 'lead text',
            'inline' => 'span',
            'selector' => 'p',
            'classes' => 'lead',
            'wrapper' => false,
        ), /*
        array(
            'title' => 'H3 Style',
            'inline' => 'span',
            'selector' => 'h2,h4,h5,h6',
            'classes' => 'title-3',
            'wrapper' => false,
            'styles' => (object)['color' => 'green']

        ),
        array(
            'title' => 'H4 Style',
            'inline' => 'span',
            'selector' => 'h2,h3,h5,h6',
            'classes' => 'title-4',
            'wrapper' => false,

        ),
        array(
            'title' => 'H5 Style',
            'inline' => 'span',
            'selector' => 'h2,h3,h4,h6',
            'classes' => 'title-5',
            'wrapper' => false,

        ),
        array(
            'title' => 'H6 Style',
            'inline' => 'span',
            'selector' => 'h2,h3,h4,h5',
            'classes' => 'title-6',
            'wrapper' => false,

        ),
        array(
            'title' => 'Quote Signature',
            'inline' => 'span',
            'selector' => 'blockquote,q',
            'classes' => 'quote-signature',
            'wrapper' => false,

        ), // */


    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );



    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );
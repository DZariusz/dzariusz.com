<?php

/**
 * for autoload classes from /class/ directory
 */
spl_autoload_register('__autoload') ;
function __autoload($className)
{

    $filePath = $className . '.php';

    $dirs = ['class/', 'class/twitteroauth-054/src/', 'class/twitteroauth-054/src/Util/'];
    $template = get_template_directory() . '/';


    foreach($dirs as $dir) {


        $dir = $template . $dir;
        $include = $dir . $filePath;
        //var_dump($include);

        //fullname
        if (file_exists($include)) {
            require_once $include;
            return;
        } //name witout `class` prefix
        elseif (substr($filePath, 0, 5) != 'class') {
            $include = $dir . 'class' . $filePath;
            //var_dump($include);
            if (file_exists($include)) {
                require_once $include;
                return;
            }
        }
    }

}
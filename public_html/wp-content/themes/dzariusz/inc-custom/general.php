<?php

define('THEME_SLUG', 'dzariusz');

function pre($var, $title = '', $die = false)
{
    echo '<pre style="text-align:left; font-size: 11px; border: 1px dashed black; border-right-width: 0; border-left-width: 0;  background-color:'."\x23DDD; color: \x23222;".'">';
    echo $title ? "<h3>$title</h3><hr>" : '';
    //print_r(esc_html($var));
    print_r($var);
    echo '</pre>';

    if ($die) die();
}

function custom_download($filename, $type = 'text/txt') {


    header('Content-Description: File Transfer');
    header('Content-Type: '. $type);
    header('Content-Disposition: attachment; filename="'.$filename.'"');
    //header('Content-Transfer-Encoding: binary');
    //header('Expires: 0');
    //header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    //header('Pragma: public');
    //header('Content-Length: ' . $size);

}


function custom_title($title, $bold) {
    return FE::__($title,'','', 'html') . FE::__($bold,'<br><strong>','</strong>', 'html');
}
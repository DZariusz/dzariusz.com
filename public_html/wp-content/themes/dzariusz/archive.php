<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage DZariusz
 * @since 1.0
 * @version 1.0
 */


global $wp_query;
$wp_query->set_404();
status_header(404);
require '404.php';
die;
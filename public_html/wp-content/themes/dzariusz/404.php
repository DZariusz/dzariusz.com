<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage DZariusz
 * @since 1.0
 * @version 1.0
 */

get_header();


?>
    <div class="basic-space"></div>

    <!-- basic-slider start -->
    <div class="basic-breadcrumb-area gray-bg ptb-70">
        <div class="container">
            <div class="basic-breadcrumb text-center">
                <h3 class="">404</h3>
            </div>
        </div>
    </div>


    <div class="breadcrumb-2-area pos-relative">
        <div class="hero-caption">
            <div class="hero-text">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 text-center wysiwyg">
                            <?php _e( 'It looks like nothing was found at this location.', 'dzariusz' ); ?>
                        </div>
                    </div>

                    <div class="view-more mt-20 text-center cb">
                        <a class="btn btn-large" href="<?= esc_url(home_url('/')); ?>">Home</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php get_footer();

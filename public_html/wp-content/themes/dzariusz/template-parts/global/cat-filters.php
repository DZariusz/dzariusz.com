<?php

$cats = get_categories([
    'echo' => false,
    'hide_empty' => true
]);
if ($cats) : ?>
    <div class="filter-menu text-center mb-40">
        <button class="active" data-filter="*"><?= __('ALL', THEME_SLUG); ?></button>
        <?php foreach ($cats as $i => $cat) : ?>
            <button data-filter=".filter-<?= $cat->slug; ?>"><?= $cat->name; ?></button>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

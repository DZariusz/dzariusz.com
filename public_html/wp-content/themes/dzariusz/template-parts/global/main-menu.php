
<header id="sticky-header" class="header-fixed">
    <div class="header-area">
        <div class="container sm-100">
            <div class="row">
                <div class="col-md-3 col-sm-2">
                    <div class="logo text-upper">
                        <h4><a href="<?= home_url( '/' ); ?>" class="dzariusz"><?= __('DZariusz'); ?></a></h4>
                    </div>
                </div>
                <div class="col-md-9 col-sm-10">
                    <div class="menu-area hidden-xs">
                        <div class="hamburger hamburger--collapse">
                            <div class="hamburger-box">
                                <div class="hamburger-inner"></div>
                            </div>
                        </div>
                        <nav class="hamburger-menu">
                            <?php if ( has_nav_menu( 'main' ) ) :
                                wp_nav_menu( array(
                                    'theme_location' => 'main',
                                    'menu_class'        => 'basic-menu clearfix',
                                    'depth'          => 2,
                                    'container' => ''
                                ) );
                            endif; ?>
                        </nav>
                    </div>
                    <!-- basic-mobile-menu -->
                    <div class="basic-mobile-menu visible-xs">
                        <nav id="mobile-nav">
                            <?php if ( has_nav_menu( 'main' ) ) :
                                wp_nav_menu( array(
                                    'theme_location' => 'main',
                                    'depth'          => 2,
                                    'container' => ''
                                ) );
                            endif; ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="basic-space"></div>

<!-- basic-slider start -->
<div class="basic-breadcrumb-area gray-bg ptb-70">
    <div class="container">
        <div class="basic-breadcrumb text-center">
            <h3 class=""><?php
            if (is_home()) {
                echo get_the_title( get_option('page_for_posts'));
            }
            else the_title();
            ?></h3>
        </div>
    </div>
</div>

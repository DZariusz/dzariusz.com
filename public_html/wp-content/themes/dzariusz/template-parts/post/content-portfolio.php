<?php
$post_format = get_post_format();
$filters = '';
foreach(get_the_category() as $cat) {
    $filters .= 'filter-'. $cat->slug . ' ';
}

?>

<div class="portfolio-item <?= $filters; ?>">
    <div class="portfolio-wrapper">
        <div class="portfolio-thumb">
            <?= FE::getFeaturedImgTag(get_the_ID(), WPthumbnails::PORTFOLIO); ?>
            <div class="view-icon">
                <a class="popup-link" href="<?= get_the_post_thumbnail_url(get_the_ID(), WPthumbnails::LARGE); ?>"><span class="icon-focus"></span></a>
            </div>
        </div>

        <div class="portfolio-caption caption-border text-center">
            <h4><a href="#"><?php the_title(); ?></a></h4>
            <?php $cta = FE::cta(get_field('portfolio_site', get_the_ID()));
            if ($cta) : ?>
                <div class="work-tag"><?= $cta; ?></div>
            <?php endif; ?>
        </div>

    </div>
</div>

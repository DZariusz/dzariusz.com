<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage DZariusz
 * @since 1.0
 * @version 1.0
 */

?>


<div class="breadcrumb-2-area pos-relative">
    <div class="hero-caption">
        <div class="hero-text">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center wysiwyg">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

$banner = GlobalElements::make()->readData('banner')->getFields();

$title = explode(',', get_the_title());


?>


<div class="hero basic-slider slide-3 height-100-vh" >
    <div class="hero-banner" <?= FE::getStyleBGimg($banner->banner, WPthumbnails::BANNER); ?>></div>
    <div class="container">
        <div class="hero-caption">
            <div class="slider-content hero-text">
                <h2 class="title cd-headline clip is-full-width"><?= $title[0]; ?>
                    <?php if (count($title) > 1) : ?>
                    <span class="cd-words-wrapper">
                        <?php foreach ($title as $k => $v) :
                            if ($k == 0) continue;
                            $class = $k == 1 ? 'is-visible' : '';
                            echo "<b class='$class'>$v</b>";
                        endforeach; ?>
                    </span>
                    <?php endif; ?>
                </h2>
                <?= FE::__($banner->subtitle, '<p>', '</p>'); ?>
                <?= FE::cta($banner, "btn"); ?>
            </div>
        </div>
    </div>
</div>

'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'), // JavaScript compressor
    concat = require('gulp-concat'), // Concatenate files 
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    gcmq = require('gulp-group-css-media-queries'),
    plumber = require('gulp-plumber'),
    removeFiles = require('del');
    //rimraf = require('rimraf');

var buffer = require('vinyl-buffer');
var csso = require('gulp-csso');
var imagemin = require('gulp-imagemin');
var merge = require('merge-stream');
var spritesmith = require('gulp.spritesmith');

var util = require('gulp-util');
var cleanCSS = require('gulp-clean-css');


var path = {
    src: {
        jquery: '_src/js/*.js',
        js: '_src/js/partials/*.js',
        css: '_src/scss/',
        style: '_src/scss/all.scss',
        editor: '_src/scss/editor-style.scss',
        sprites: '_src/sprites/**/*.{png,jpg}',
        fonts: '_src/fonts/*.*',
        images: '_src/images/**/*.*'
    },

    build: {
        jquery: 'public_html/wp-content/themes/dzariusz/assets/js/',
        js: 'public_html/wp-content/themes/dzariusz/assets/js/',
        css: 'public_html/wp-content/themes/dzariusz/assets/css/',
        sprites: 'public_html/wp-content/themes/dzariusz/assets/images/',
        fonts: 'public_html/wp-content/themes/dzariusz/assets/fonts/',
        images: 'public_html/wp-content/themes/dzariusz/assets/images/'
    },

    watch: {
        jquery: '_src/js/*.js',
        js: '_src/js/**/*.js',
        css: '_src/scss/**/*.scss',
        sprites: '_src/sprites/**/*.*{png,jpg}',
        fonts: '_src/fonts/**/*.*',
        images: '_src/images/**/*.*'
    },
    clean: {
        fonts: './public_html/wp-content/themes/dzariusz/assets/fonts/*',
        css: './public_html/wp-content/themes/dzariusz/assets/css/*',
        js: './public_html/wp-content/themes/dzariusz/assets/js/*',
        images: './public_html/wp-content/themes/dzariusz/assets/images/*'
    }
};

var config = {
    server: {
        baseDir: "./_src"
    },
    tunnel: true,
    host: 'localhost',
    port: 9000,
    logPrefix: "Frontend",
    production: !!util.env.prod // gulp --prod
};

gulp.task('clean', function(){

    //console.log(path.clean.folders);
    return removeFiles([path.clean.fonts, path.clean.js, path.clean.css, path.clean.images]);

});


gulp.task('js:build', function () {
    gulp.src([ path.src.js ])
        .pipe(plumber())
        .pipe(concat('scripts.js'))
        .pipe(config.production ? uglify() : util.noop())
        .pipe(gulp.dest( path.build.js ));
});

// generate sprite.png and _sprite.scss
gulp.task('sprites:build', function () {

    var options = {
        imgName: 'sprites.png',
        cssName: '_sprite.scss',
        //cssFormat: 'sass',
        algorithm: 'top-down',
        padding: 1,
        imgPath: '../images/sprites.png',
        cssVarMap: function (sprite) {
            sprite.name = 'icon-' + sprite.name;
        },
        cssOpts: {
            cssSelector: function (sprite) {
                return '.'+ sprite.name.replace('-hover', ':hover');
            } /* ,
             cssClass: function (item) {
             // If this is a hover sprite, name it as a hover one (e.g. 'home-hover' -> 'home:hover')
             return '.xxx-icon-' + item.name.replace('-hover', ':hover');

             if (item.name.indexOf('-hover') !== -1) {
             return '.icon-' + item.name.replace('-hover', ':hover');
             // Otherwise, use the name as the selector (e.g. 'home' -> 'home')
             } else {
             return '.icon-' + item.name;
             }
             } // */
        }

    };

    // Generate our spritesheet
    var spriteData = gulp.src( path.src.sprites ).pipe(spritesmith(options));


    // Pipe image stream through image optimizer and onto disk
    var imgStream = spriteData.img
    // DEV: We must buffer our stream into a Buffer for `imagemin`
        .pipe(buffer())
        .pipe(imagemin())
        .pipe(gulp.dest( path.build.sprites ));


    options['cssName'] = '_sprite_css.css';
     var cssData = gulp.src( path.src.sprites ).pipe(spritesmith(options));


     // Pipe CSS stream through CSS optimizer and onto disk
    var cssStream = cssData.css
        //.pipe(csso())
        .pipe(gulp.dest( path.src.css ));
     //*/

    var scssStream = spriteData.css
     .pipe(gulp.dest( path.src.css ));

    // Return a merged stream to handle both `end` events
    return merge(imgStream, scssStream, cssStream);
    //return imgStream;
});


gulp.task('style:build', function () {
    gulp.src( path.src.style )
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['_src/scss/'],
            sourcemaps: true,
            errLogToConsole: true
        }))
        .pipe(prefixer({
            browsers: ['last 2 versions', 'ie 9'],
            cascade: false
        }))
        .pipe(gcmq())
        .pipe(config.production ? cleanCSS({compatibility: 'ie10'}) : util.noop())
        .pipe(config.production ? util.noop() : sourcemaps.write())
        .pipe(gulp.dest( path.build.css ));
});

gulp.task('editor:build', function () {
    gulp.src( path.src.editor )
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: ['_src/scss/'],
            sourcemaps: false,
            errLogToConsole: true
        }))
        .pipe(prefixer({
            browsers: ['last 2 versions', 'ie 9'],
            cascade: false
        }))
        .pipe(gcmq())
        .pipe(config.production ? cleanCSS({compatibility: 'ie10'}) : util.noop())
        .pipe(config.production ? util.noop() : sourcemaps.write())
        .pipe(gulp.dest( path.build.css ));
});




gulp.task('copy', function () {
    gulp.src( path.src.fonts )
        .pipe(gulp.dest( path.build.fonts ));

    gulp.src( path.src.images )
        .pipe(gulp.dest( path.build.images ));

    gulp.src([ path.src.jquery ])
        .pipe(gulp.dest( path.build.jquery ));
});
//

gulp.task('build', [
    'sprites:build',
    'style:build',
    'editor:build',
    'js:build',
    'copy'

]);

gulp.task('watch', function(){

    gulp.watch(path.watch.sprites, ['sprites:build']);
    gulp.watch(path.watch.css, ['style:build']);
    gulp.watch(path.watch.css, ['editor:build']);
    gulp.watch(path.watch.js, ['js:build']);
    gulp.watch(path.watch.fonts, ['copy']);
    gulp.watch(path.watch.images, ['copy']);
});

gulp.task('default', ['clean', 'build', 'watch']);
